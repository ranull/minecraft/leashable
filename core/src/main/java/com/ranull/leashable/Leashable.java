package com.ranull.leashable;

import com.ranull.leashable.listener.PlayerInteractEntityListener;
import com.ranull.leashable.nms.NMS;
import org.bstats.bukkit.Metrics;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class Leashable extends JavaPlugin {
    private NMS nms;

    @Override
    public void onEnable() {
        if (setupNMS()) {
            registerMetrics();
            registerListeners();
        } else {
            getLogger().severe("Version not supported, disabling plugin!");
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onDisable() {
        unregisterListeners();
    }

    private void registerMetrics() {
        new Metrics(this, 14618);
    }

    public void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerInteractEntityListener(this), this);
    }


    public void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }

    public NMS getNMS() {
        return nms;
    }

    private boolean setupNMS() {
        try {
            String version = getServer().getClass().getPackage().getName().split("\\.")[3];
            Class<?> clazz = Class.forName("com.ranull.leashable.nms.NMS_" + version);

            if (NMS.class.isAssignableFrom(clazz)) {
                nms = (NMS) clazz.newInstance();
            }

            return nms != null;
        } catch (ArrayIndexOutOfBoundsException | ClassNotFoundException | InstantiationException
                 | IllegalAccessException ignored) {
            return false;
        }
    }
}
